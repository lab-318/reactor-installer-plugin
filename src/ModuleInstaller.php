<?php

namespace Thortech\Composer;

use Composer\Package\PackageInterface;
use Composer\Installer\LibraryInstaller;
use InvalidArgumentException;

class ModuleInstaller extends LibraryInstaller
{
    /**
     * {@inheritDoc}
     */
    public function getInstallPath(PackageInterface $package)
    {
        $prettyName = $package->getPrettyName();
        $isMatching = preg_match('/([^\/]+)\/module\-(.+)/', $prettyName, $matches);

        if (!$isMatching) {
            throw new InvalidArgumentException(
                'Unable to install module, '
                    . 'reactor module should always follow package name pattern '
                    . '"<vendor>/module-<module-name>".'
            );
        }

        return "modules/{$matches[1]}-{$matches[2]}";
    }

    /**
     * {@inheritDoc}
     */
    public function supports($packageType)
    {
        return 'thortech-reactor-module' === $packageType;
    }
}
